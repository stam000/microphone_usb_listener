import time
import threading
import numpy as np
import serial
import queue

class AudioAcquisition:
    def __init__(self,port=None, processingWindowTimeInterval=1,
                    sampleRate=150000):
        self._port = port
        self._processingWindowTimeInterval = processingWindowTimeInterval
        self.sampleRate = sampleRate
        self.acquisitionTime = 1/sampleRate
        self.processingWindowSize = int(processingWindowTimeInterval * sampleRate)
        self.processingBuffer = np.zeros(self.processingWindowSize,dtype='int16')
        self._thread = threading.Thread(target=self._callback)
        self.serial = serial.Serial(port=port)
        self.dataQueue = queue.Queue(maxsize=2)

    def _callback(self):
        while 1:
            inData = np.frombuffer(self.serial.read(self.processingBuffer.nbytes), dtype='int16')
            try:
                self.dataQueue.put_nowait(inData)
            except queue.Full:
                self.dataQueue.get()
                self.dataQueue.put_nowait(inData)


    def getPacket(self):
        return self.dataQueue.get()

    def start(self):
        self._thread.start()

    def stop(self):
        self._thread.stop()


