
from numpy import cos, sin, pi, absolute, arange
from scipy.signal import kaiserord, lfilter, firwin, freqz
from pylab import figure, clf, plot, xlabel, ylabel, xlim, ylim, title, grid, axes, show
import matplotlib.pyplot as plt
import numpy as np
sample_rate = 4.8E6
#------------------------------------------------
# Create a FIR filter and apply it to x.
#------------------------------------------------

# The Nyquist rate of the signal.
nyq_rate = sample_rate / 2.0

# The desired width of the transition from pass to stop,
# relative to the Nyquist rate.
width = 172E3/nyq_rate


# The desired attenuation in the stop band, in dB.
ripple_db = 40

# Compute the order and Kaiser parameter for the FIR filter.
N, beta = kaiserord(ripple_db, width)

# The cutoff frequency of the filter.
cutoff_hz = 60E3


# Use firwin with a Kaiser window to create a lowpass FIR filter.
taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))
(np.round(taps*10000,0).astype('int16'))
len(taps)
taps = (taps/taps.min()).astype('uint16')
print(taps)
print(len(taps))
# Use lfilter to filter x with the FIR filter.
# filtered_x = lfilter(taps, 1.0, x)