import threading
import time
import serial
import struct
import numpy as np
import queue
import sys
import io
from scipy import signal

class MicAcquisition:
    def __init__(self,port,sampleRate,packetSize,queueMaxSize=100,acquisitionInterval=0.5,minMicFrequency=18E3,maxMicFrequency=60E3):
        self.acquisitionInterval = acquisitionInterval
        self._usbMicSerial = serial.Serial( port=port,timeout=acquisitionInterval,)
        self.sampleRate = sampleRate
        self._aquireMicThreadControl = threading.Thread(target=self._aquireMicThread)
        self._isRecordingOn = False
        self._queueMaxSize = queueMaxSize
        self._bufferQueue = queue.Queue(maxsize=self._queueMaxSize)
        self.sampleRate = sampleRate
        self.packetSize = packetSize
        self.acquisitionInterval = packetSize/sampleRate
        self.butter_b, self.butter_a = signal.butter(4, [minMicFrequency,maxMicFrequency], fs=300000, btype='bandpass')

    def startRecording(self):
        self._isRecordingOn = True
        self._aquireMicThreadControl.start()

    def stopRecording(self):
        self._isRecordingOn = False

    def _aquireMicThread(self):
        self._usbMicSerial.reset_output_buffer()
        self._usbMicSerial.reset_input_buffer()
        while self._isRecordingOn:
            acquisitionStart = time.time()
            try:
                inSignal = np.frombuffer(self._usbMicSerial.read(100000), dtype='int8')
                if len(inSignal) > 0:
                    self._bufferQueue.put_nowait(signal.lfilter(self.butter_b, self.butter_a, inSignal).astype('float32'))
            except Exception:
                pass
            acquisitionEnd = time.time()
            self.acquisitionInterval = self.acquisitionInterval * (1-0.3) + (acquisitionEnd-acquisitionStart) * 0.3
    def getPacket(self):
        return self._bufferQueue.get()

    def clearBuffer(self):
        self._bufferQueue = queue.Queue(maxsize=self._queueMaxSize)

    def availablePackets(self):
        return self._bufferQueue.qsize()


#tester functions
import matplotlib.pyplot as plt
def testRecordingToFile():
    mic = MicAcquisition(port='COM4', packetTimeInterval=0.1, persistenceTimeInterval=3,
                                       sampleRate=4.8E6)
    mic.startRecording()
    pass

#
# if __name__ == "__main__":
#     testRecordingToFile()
