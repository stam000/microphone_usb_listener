import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.stats import sigmaclip
from scipy.signal import butter, lfilter, freqz

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

sampleRate=140000
Noverlap = 256
NFFT=512

audio = np.load(r'RecordedData\2022-08-20 18-36-28.npy')
audio = sigmaclip(audio,low=4,high=4)[0]
arr = plt.mlab.specgram(audio, Fs=sampleRate,NFFT=NFFT,noverlap=Noverlap)[0]
# filtered = butter_lowpass_filter(arr,50000,140000,)
im = plt.imshow(arr, animated=True,origin='lower',norm=LogNorm(vmin=0.01, vmax=0.8))