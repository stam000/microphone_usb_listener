import threading
import numpy as np
from audiotsm.io.array import ArrayReader, ArrayWriter
import sounddevice as sd
import time
from scipy import signal
import sys
import queue

start_idx = 0

class SpeakerEcho:
    def __init__(self, mic, TSM, speed,powerScaler,outputDevice=None,synthesisRate=44100,):
        self._mic = mic
        self._synthesisRate = synthesisRate
        self._speed = speed
        self.TSM = TSM
        self.powerScaler = powerScaler
        self.TSM.set_speed(speed)
        self._echoMicThreadControl = threading.Thread(target=self._echoMicThread)
        self.audioBatchLength = int(1E4)
        self.dataToSend = ''
        self.outputDevice = outputDevice
        self._queue = queue.Queue(maxsize=50)
        self._stream = sd.OutputStream(device=self.outputDevice,
                                       samplerate=self._synthesisRate,blocksize=22050,
                                       channels=1, callback=self._DACcallback,)

    def start(self):
        self._echoMicThreadControl.start()
        self._stream.start()

    def _DACcallback(self, outdata, frames, time, status):
        outdata[:] = self._queue.get().reshape(-1,1)

    def _echoMicThread(self):
        time.sleep(1)
        while 1:
            correctedSpeed = self._speed * (self._synthesisRate/self._mic.sampleRate)
            self.TSM.set_speed(correctedSpeed)
            wantedLength = 45444

            buf = self._mic.processingBuffer[:wantedLength]
            pitchedDown = self._generateTSM(buf)
            pitchedDownResampled = signal.resample(pitchedDown,int(0.5*44100))
            pitchedDownFilteredRescaled = pitchedDownResampled*self.powerScaler
            self._queue.put(pitchedDownFilteredRescaled)
    def _generateTSM(self,audioDataWindow):
        audioDataWindow = audioDataWindow.reshape((1, -1))
        reader = ArrayReader(audioDataWindow)
        writer = ArrayWriter(channels=1)
        self.TSM.run(reader, writer)
        return writer.data[0]
