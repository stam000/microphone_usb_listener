import struct
from enum import auto

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from matplotlib.colors import LogNorm
import sys
from datetime import datetime
from scipy.stats import sigmaclip

class SpectogramVisualizer:
    Noverlap = 256
    NFFT = 512

    def __init__(self, mic,NFFT=NFFT):
        self._mic = mic
        self.sampleRate = mic.sampleRate
        self.NFFT = NFFT

    def start(self):
        self._init_plots()
        self._ani = animation.FuncAnimation(self.fig,self.animate,interval=100,blit=True)
        self.fig.canvas.mpl_connect('key_press_event', self.on_press)
        plt.ion()
        plt.show()

    def _init_plots(self):
        self.fig, self.ax1 = plt.subplots()
        audioDataWindow = self._mic.getPacket()
        arr = plt.mlab.specgram(audioDataWindow, Fs=self.sampleRate,NFFT=self.NFFT,noverlap=self.Noverlap)[0]
        self.im = plt.imshow(arr, animated=True,origin='lower',norm=LogNorm(vmin=0.01, vmax=0.8))

    def animate(self,i):
        audioDataWindow = self._mic.getPacket()
        self.audioCopyForRecordingEvent = audioDataWindow.copy()
        audioDataWindow = sigmaclip(audioDataWindow, low=4, high=4)[0]
        arr = plt.mlab.specgram(audioDataWindow, Fs=self.sampleRate,NFFT=self.NFFT,noverlap=self.Noverlap)[0]
        maxNoise = arr[64:].max()
        if maxNoise > 0.04:
            self.im.set_array(arr)
        return self.im,

    def on_press(self,event):
        sys.stdout.flush()
        if event.key == 'x':
            print('saving image and data')
            curr_datetime = datetime.now().strftime('%Y-%m-%d %H-%M-%S')
            self.fig.savefig(r'RecordedData\{}.jpg'.format(curr_datetime))
            np.save(r'RecordedData\{}.npy'.format(curr_datetime),self.audioCopyForRecordingEvent)
