import serial
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
ser = serial.Serial(port='COM12',timeout=100)

fig,ax = plt.subplots()
line, = ax.plot(np.arange(0,512),np.zeros(512))

def animation_function(i):
    try:
        data = np.array(ser.readline().strip().decode().split(',')[:-1],dtype='float')
        # print(data)
        line.set_ydata(data)
    except ValueError:
        pass
    return line,
#
#
animation = FuncAnimation(fig,func = animation_function,interval=10)
plt.show()
