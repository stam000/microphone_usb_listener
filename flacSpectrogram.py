

import numpy as np
import soundfile as sf
import scipy.signal as signal
import matplotlib.pyplot as plt


data, samplerate = sf.read(r"DeepSqueak-master\Audio\Example Rat Recording.flac")


offset = int(0.5E6)
width = int(3.5E6)
data = data[offset:offset+width]
#
# freq, time, Sxx = signal.spectrogram(data, samplerate, scaling='spectrum')
# plt.pcolormesh(time, freq, Sxx)
# plt.ylabel('Frequency [Hz]')
# plt.xlabel('Time [sec]')
b, a = signal.butter(4, [16000, 70000], fs=samplerate, btype='bandpass')
dataFiltered = signal.lfilter(b, a, data).astype('float32')
Pxx, freqs, bins, im = plt.specgram(dataFiltered, Fs=samplerate)


# add axis labels
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')

squeak = dataFiltered[int(samplerate*5.884):int(samplerate*5.940)]
squeak_normalized = (squeak/squeak.ptp()+0.5) * ((2**16) - 1)
np.savetxt('squeak_test.csv',squeak_normalized,delimiter='\n',fmt='%d')