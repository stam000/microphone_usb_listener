import struct
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from scipy import signal

import audiotsm
import SpectrogramVisualizer
import SpeakerEcho
import AudioAcquisition
from pynput.keyboard import Key, Listener
import threading

# The parameters of the input signal
length = 1  # in seconds
mic = AudioAcquisition.AudioAcquisition(processingWindowTimeInterval=0.5,sampleRate=140000,port='COM3')
mic.start()

vis = SpectrogramVisualizer.SpectogramVisualizer(mic)
vis.start()


# echo = SpeakerEcho.SpeakerEcho(mic,TSM=audiotsm.wsola(channels=1),speed=20,powerScaler = 5,sampleRate=125000,synthesisRate=44100)
# echo.start()
