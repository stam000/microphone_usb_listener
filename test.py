import serial
import numpy as np
import matplotlib.pyplot as plt


ser = serial.Serial('COM12')

buf = np.zeros(1000,dtype='int16')
buf = np.frombuffer(ser.read(buf.nbytes),dtype='int16')
plt.plot(buf)